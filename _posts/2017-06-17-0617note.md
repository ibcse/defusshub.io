---
title: "Meeting note 0617"
layout: post
category: document
tags: [meeting]
excerpt: "meeting"
---

# 2017.06.17 note
----

* Brain storming

    1. Drawing Charts
    2. Change Chart Types
    3. Drawing Category
    4. Drawing Axis
    5. Drawing Grid 
    6. Transforming Animation
    7. Loading Animation
    8. Change Value 
    9. Change Axis
    10. Data Label
    11. Legend
    12. Axis Name Label
    13. Category Scale
    14. 3D Effects
    15. Change Themes
    16. Setting Chards (Style)
    17. Responsive Chart
    
* Critical Item
    1. Drawing Charts
    2. Drawing Category
    3. Drawing Axis
    4. Data Label
    5. Legend
    6. Axis Name Label
* Important Item
    1. Change Chart Types
    2. Drawing Grid 
    3. Change Value 
    4. Change Axis
    5. Chart Settings
* Useful Item
    1. Transform Animation
    2. Loading Animation
    3. Scalable Categories
    4. 3D effect
    5. Theme
    6. Responsive Chart

# Usecase
## Drawing Charts
    1. Set Value Data
    2. Set Chart Data
    3. Draw Chart
        3.1. Draw Categories
        3.2. Draw Axis
        3.3. Data Value Labele
        3.4. Draw Legend
        3.5. Axis Value Labele
        3.6. Draw Series - path, bar 

## Change Chart Types

## Drawing Grids

## Change Axis and Data

## Chart Settings

## Animations

## Theme 

## Effects

## Scalable Categories
